'use strict';

const express = require('express');
const path = require('path');

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'));
})

app.get('/info', (req, res) => {
  res.sendFile(path.join(__dirname, 'info.html'));
})

app.listen(PORT);
console.log(`Listening on port ${PORT}`);

