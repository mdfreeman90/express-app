FROM node:fermium-alpine3.14

ARG PORT=8080

WORKDIR /src/usr/app

COPY package.json server.js index.html info.html ./

RUN npm install

EXPOSE 8080

ENTRYPOINT [ "node", "server.js" ]
